'''BlackBerry-Tart support code including Application class.'''

import sys
import pickle
import traceback

import _tart
import tart
from .decorators import cached_property
from .engine import Engine


class Application:
    '''Tart Application object for the Python backend.'''

    def __init__(self, debug=False):
        self.debug = debug
        if self.debug:
            tart.log('tart: app starting')

        self.engine = Engine(self)


    def start(self):
        '''entry point to main loop'''
        if self.debug:
            print('calling _tart.event_loop()')

        # Call back into C++ so it can call QThread::exec() and process
        # the Qt event loop, handling incoming signals and events and such.
        # This should never return except via a SystemExit exception,
        # which will be caught by blackberry_tart.py.
        _tart.event_loop(self.engine.handle_event)

        if self.debug:
            print('returned from _tart.event_loop() (should never see this)')


    def restore_data(self, data, path):
        '''Utility function to retrieve persisted data,
        if any, and restore only those items we currently support.
        This should probably be broken out to an optional and separate
        support package but, for now, here it is...
        '''
        try:
            saved = pickle.load(open(path, 'rb'))
        except:
            saved = {}

        # restore only recognized items, which means we'll ignore the
        # version key for now
        for key in data:
            try:
                data[key] = saved[key]
                if self.debug:
                    print('{}: restored {} = {!r}'.format(
                        path,
                        key,
                        data[key],
                        ))
            except KeyError:
                pass


    def save_data(self, data, path):
        '''See restore_data() or samples that use this.'''
        # we can get fancier later when we need
        data['version'] = 1

        if self.debug:
            print('{}: persisting {!r}'.format(path, data))
        pickle.dump(data, open(path, 'wb'))


    def onUiReady(self):
        '''Sent when the QML has finished onCreationCompleted for the root
        component.  Override in subclasses as required.'''
        pass


    def onManualExit(self):
        '''Sent when the app is exiting, so we can save state etc.
        Override in subclasses as required.'''
        tart.send('continueExit')
        # sys.exit(1)


    #---------------------------------------------
    # Transparently create BPS event dispatcher when it's requested,
    # allowing us to hook into the event feed with a Qt event filter
    @cached_property
    def bps_dispatcher(self):
        from .bps_dispatcher import BpsEventDispatcher
        return BpsEventDispatcher(self.debug)


    #---------------------------------------------
    # Transparently create Clipboard when it's requested,
    # allowing us to access the system clipboard data.
    @cached_property
    def clipboard(self):
        from .clipboard import Clipboard
        return Clipboard()


    #---------------------------------------------
    # Useful things with which to help JavaScript debugging.
    # These can be called from the command line interface via
    # telnet, or in some automated testing.  Not used but
    # left here for now, to inspire future ideas...

    def js(self, text):
        '''send to JavaScript for evaluation'''
        tart.send('evalJavascript', text=text)


    def onEvalResult(self, value=None):
        '''result from JavaScript eval sent from js()'''
        print('result', value)


# EOF
