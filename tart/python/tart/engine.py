'''Tart event engine.'''

import itertools
import json
import traceback
import types

import tart
from .decorators import cached_property


class RequestError(Exception):
    '''QML response to engine.request() was an error result.'''


class Key(int):
    '''Subclass of int used to flag our magic keys, auto-generated.'''
    def __new__(cls, value=None, _count=itertools.count(1).__next__):
        if value is None:
            value = _count()
        return super(Key, cls).__new__(cls, value)


    def __repr__(self):
        return 'Key(%d)' % self



class Engine:
    def __init__(self, app, debug=False):
        self.debug = debug
        self.app = app
        self.generators = {}


    def handle_event(self, event=''):
        '''called from event loop to handle'''
        if self.debug:
            tart.log('tart: event', event)

        try:
            msg = json.loads(event)
        except ValueError:
            msg = []

        # extract message type and build handler name based on convention
        # shared with and adopted from QML
        try:
            msg_type = msg[0]

            # special (provisional?) handling of messages from QML that
            # are responses to engine.request() yields
            if msg_type == '_response':
                self.handle_response(msg[1])
                return

            # apply Qt-style case normalization
            name = 'on' + msg_type[0].upper() + msg_type[1:]
        except IndexError:
            tart.log('tart: ERROR, no type found in message')
            return

        else:
            # find a matching handler routine, if there is one
            try:
                handler = getattr(self.app, name)
            except AttributeError:
                if msg_type.startswith('on'):
                    tart.log('tart: WARNING, message starts with "on", maybe remove?')

                self.missing_handler(msg)
                return

        # tart.log('calling', handler)
        try:
            kwargs = msg[1] or {}
        except KeyError:
            kwargs = {}

        # Actually process the message in the handler: note that
        # results are ignored for now, and any exceptions will
        # result in a traceback from the calling code (in tart.cpp)
        result = handler(**kwargs)

        if isinstance(result, types.GeneratorType):
            self.resume(gen=result)
        elif isinstance(result, Key):
            gen = self.generators.pop(result)
            self.resume(gen=gen)


    def missing_handler(self, msg):
        tart.log('tart: ERROR, missing handler for', msg[0])


    def handle_response(self, data):
        print('handle_response', repr(data))
        try:
            msgid = data.pop('_msgid')
        except KeyError:
            tart.log('engine error: _msgid missing from QML _response')
            return

        self.resume(msgid, data=data.get('data'), error=data.get('error'))


    def resume(self, key=None, gen=None, data=None, error=None):
        if not gen:
            gen = self.generators.pop(key)
            print('resuming %r for %r, data %r, error %r' % (gen, key, data, error))
        else:
            print('starting %r, data %r' % (gen, data))

        try:
            if error is not None:
                # FIXME: this is almost certainly a bunch of cruft, but I want
                # something working so we can explore how we'll use it, rather
                # than something clean that won't do what we may want.
                if not isinstance(error, tuple) or not (1 <= len(error) <= 3) or not isinstance(error[0], Exception):
                    error = (Exception, error)  # use as value for an exception

                yielded = gen.throw(*error)
            else:
                yielded = gen.send(data)

        except StopIteration:
            return

        # if a Key is yielded, set up to resume the generator later
        if isinstance(yielded, Key):
            if yielded in self.generators:
                raise KeyError('duplicate key yielded: %s' % yielded)
            print('storing state, %r' % yielded)
            self.generators[yielded] = gen
        else:
            raise TypeError('unable to handle yielded type: %s' % type(yielded))


    @cached_property
    def timer_handler(self):
        from .timing import TimerHandler
        return TimerHandler(debug=self.debug)


    def delay(self, duration):
        key = Key()

        def timer_done():
            print('timer_done called %r' % key)
            self.resume(key)
        self.timer_handler.start_timer(duration, timer_done)

        return key


    def request(self, type, **kwargs):
        key = kwargs['_msgid'] = Key()
        tart.send(type, **kwargs)
        return key


    def chain(self, generator):
        key = Key()
        self.generators[key] = generator
        return key


# EOF
