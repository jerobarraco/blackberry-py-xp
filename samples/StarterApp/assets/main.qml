import bb.cascades 1.0
import bb.system 1.0
import "tart.js" as Tart

NavigationPane {
    id: root

    property variant settingsPage
    property variant helpPage

    signal settingsChanged()

    Page {
        id: mainPage

        Container {
            layout: DockLayout {}

            Label {
                id: kbmsg
                visible: false
                text: qsTr("Look out! It's the keyboard!!")
                horizontalAlignment: HorizontalAlignment.Left
                verticalAlignment: VerticalAlignment.Top
            }

            Label {
                id: label
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                multiline: true
                text: qsTr("Wait for it...")
                textStyle.fontSize: FontSize.PercentageValue
                textStyle.fontSizeValue: 150
                textStyle.textAlign: TextAlign.Center
            }
        }

        actions: [
            ActionItem {
                title: "Yield Demo"
                ActionBar.placement: ActionBarPlacement.OnBar
                onTriggered: {
                    Tart.send('yieldDemo', {delay: 1.2});
                }
            }
        ]
    }

    attachedObjects: [
        ComponentDefinition {
            id: helpDef
            source: "HelpPage.qml"
        }
        ,
        ComponentDefinition {
            id: settingsDef
            source: "SettingsPage.qml"
        }
        ,
        QtObject {
            id: settings

            property bool metric: true

            onCreationCompleted: {
                // TODO: handle dynamic changes to this
                var system = app.getLocaleInfo('measurementSystem');
                metric = (system == 'metric');
            }

            onMetricChanged: {
                settingsChanged();
            }

            function restore(data) {
                print('restoring', Object.keys(data));
                if (data.metric != null) {
                    print('metric =', data.metric);
                    metric = data.metric;
                }
            }
        }
        ,
        // TODO: make this a three-button dialog so we can say Yes/No/Cancel
        // to help us finish the generator resume support with exceptions
        // raised into the generator via throw()
        SystemDialog {
            id: dialog
            property int msgid

            confirmButton.label: qsTr("Yes")
            confirmButton.enabled: true
            cancelButton.label: qsTr("Cancel")
            cancelButton.enabled: true

            buttons: [
                SystemUiButton {
                    label: qsTr("No")
                    enabled: true
                }
            ]

            onFinished: {
                if (dialog.result == SystemUiResult.CancelButtonSelection)
                    Tart.send('_response', {_msgid: dialog.msgid, error: 'cancel'});
                else
                    Tart.send('_response', {_msgid: dialog.msgid, data:
                        dialog.result == SystemUiResult.ConfirmButtonSelection});
            }
        }
        ,
        SystemToast {
            id: toast
        }
    ]

    Menu.definition: MenuDefinition {
        settingsAction: SettingsActionItem {
            onTriggered: {
                settingsPage = settingsDef.createObject();
                root.push(settingsPage);
                Application.menuEnabled = false;    // re-enable when popped
            }
        }
        helpAction: HelpActionItem {
            onTriggered: {
                Tart.send('getHelp');
            }
        }
    }

    function onRestoreSettings(data) {
        settings.restore(data);
    }

    function onGotHelp(data) {
        helpPage = helpDef.createObject();
        helpPage.text = data.text;
        root.push(helpPage);
        Application.menuEnabled = false;    // re-enable when popped
    }

    // handle keyboard state events from BPS
    function onKeyboardState(state) {
        kbmsg.visible = state.visible;
    }

    function onDialogYesNo(data) {
        print('dialog');
        dialog.msgid = data._msgid;
        dialog.body = data.text;
        print('dialog.show', JSON.stringify(data));
        dialog.show();
        print('dialog.show done');

        label.visible = false;
    }

    function onToast(data) {
        toast.body = data.text;
        toast.show();
    }


    onSettingsChanged: {
        var data = {
            metric: settings.metric
        };

        Tart.send('saveSettings', {settings: data});
    }

    onPopTransitionEnded: {
        if (page == settingsPage)
            Application.menuEnabled = true;
        else
        if (page == helpPage)
            Application.menuEnabled = true;

        page.destroy();
    }

    onCreationCompleted: {
        Tart.debug = true;
        Tart.init(_tart, Application);

        Tart.register(root);

        Tart.send('uiReady');
    }
}
