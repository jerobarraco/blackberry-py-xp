import os
import sys

import tart
from tart import vkb_handler


class App(tart.Application):
    SETTINGS_FILE = 'data/app.state'

    def __init__(self):
        super().__init__(debug=True)   # set True for some extra debug output

        # To ensure obsolete and unrecognized settings are not restored in
        # later versions (if you change stuff and a user updates an older
        # version of the app) we restore only things that actually have
        # entries here.  Prepopulate this with default values, or use None
        # and make sure that case is handled in the QML to preserve its defaults.
        self.settings = {
            'metric': None,
        }
        self.restore_data(self.settings, self.SETTINGS_FILE)


    def onUiReady(self):
        # need to defer sending this, for now, until the event loop has started
        tart.send('restoreSettings', **self.settings)

        # install BPS event handler for vkb events, which for now reports
        # "keyboardState" events with boolean property "visible"
        vkb_handler.VkbHandler(self.bps_dispatcher)

        # # chain to a generator (only at end of function)
        # return self.engine.chain(self.onYieldDemo())

        gen = self.onYieldDemo()
        _s = None
        while 1:
            try:
                _y = gen.send(_s)
            except StopIteration:
                break

            try:
                _s = yield _y
            except GeneratorExit as ex:
                gen.close()
                raise ex
            except BaseException:
                try:
                    _y = gen.throw(*sys.exc_info())
                except StopIteration:
                    break


    def onYieldDemo(self, delay=3.5):
        yield self.engine.delay(delay)

        try:
            result = yield self.engine.request('dialogYesNo', text='Do you like it so far?')
        except Exception:
            # TODO: support this, once we figure out how to have a three-button
            # SystemDialog
            print('user must have cancelled')
            tart.send('toast', text='Fence-sitter!')

        else:
            print('user response', result)
            if result:
                tart.send('toast', text='Glad to hear it!')
            else:
                tart.send('toast', text='Sorry to hear it!')


    def onSaveSettings(self, settings):
        self.settings.update(settings)
        self.save_data(self.settings, self.SETTINGS_FILE)


    def onGetHelp(self):
        helppath = os.path.join(os.path.dirname(__file__), '../assets/help.html')
        with open(helppath, encoding='utf-8') as f:
            tart.send('gotHelp', text=f.read().strip().replace('\n', ' '))
